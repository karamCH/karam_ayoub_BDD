-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 16 jan. 2022 à 09:34
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cqc`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartment`
--

DROP TABLE IF EXISTS `appartment`;
CREATE TABLE IF NOT EXISTS `appartment` (
  `id_appart` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `security_level` int(11) DEFAULT NULL,
  `id_lodger` int(11) DEFAULT NULL,
  `id_house` varchar(50) NOT NULL,
  PRIMARY KEY (`id_appart`),
  KEY `id_lodger` (`id_lodger`),
  KEY `id_house` (`id_house`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `appartment_piece`
--

DROP TABLE IF EXISTS `appartment_piece`;
CREATE TABLE IF NOT EXISTS `appartment_piece` (
  `id_type_appartment` int(11) NOT NULL,
  `id_type_piece` int(11) NOT NULL,
  PRIMARY KEY (`id_type_appartment`,`id_type_piece`),
  KEY `id_type_piece` (`id_type_piece`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `asso_12`
--

DROP TABLE IF EXISTS `asso_12`;
CREATE TABLE IF NOT EXISTS `asso_12` (
  `id_appart` int(11) NOT NULL,
  `id_type_appartment` int(11) NOT NULL,
  PRIMARY KEY (`id_appart`,`id_type_appartment`),
  KEY `id_type_appartment` (`id_type_appartment`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id_city` varchar(50) NOT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `id_department` varchar(50) NOT NULL,
  PRIMARY KEY (`id_city`),
  KEY `id_department` (`id_department`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `consume`
--

DROP TABLE IF EXISTS `consume`;
CREATE TABLE IF NOT EXISTS `consume` (
  `id_ressource` int(11) NOT NULL,
  `ide_device_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ressource`,`ide_device_type`),
  KEY `ide_device_type` (`ide_device_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id_department` varchar(50) NOT NULL,
  `department_name` varchar(50) DEFAULT NULL,
  `id_region` varchar(50) NOT NULL,
  PRIMARY KEY (`id_department`),
  KEY `id_region` (`id_region`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `device`
--

DROP TABLE IF EXISTS `device`;
CREATE TABLE IF NOT EXISTS `device` (
  `id_device` varchar(50) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `piece` int(11) DEFAULT NULL,
  `place_description` varchar(30) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `ide_device_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_device`),
  KEY `ide_device_type` (`ide_device_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
CREATE TABLE IF NOT EXISTS `device_type` (
  `ide_device_type` varchar(50) NOT NULL,
  `defamation` varchar(50) NOT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `work_times_per_day` varchar(50) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ide_device_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `have`
--

DROP TABLE IF EXISTS `have`;
CREATE TABLE IF NOT EXISTS `have` (
  `id_device` varchar(50) NOT NULL,
  `id_appart` int(11) NOT NULL,
  PRIMARY KEY (`id_device`,`id_appart`),
  KEY `id_appart` (`id_appart`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `house`
--

DROP TABLE IF EXISTS `house`;
CREATE TABLE IF NOT EXISTS `house` (
  `id_house` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `citizenship_level` int(11) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `adress` varchar(50) DEFAULT NULL,
  `isolation_degree` int(11) DEFAULT NULL,
  `id_city` varchar(50) NOT NULL,
  `id_owner` int(11) NOT NULL,
  PRIMARY KEY (`id_house`),
  KEY `id_city` (`id_city`),
  KEY `id_owner` (`id_owner`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `issue`
--

DROP TABLE IF EXISTS `issue`;
CREATE TABLE IF NOT EXISTS `issue` (
  `id_substance` int(11) NOT NULL,
  `ide_device_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_substance`,`ide_device_type`),
  KEY `ide_device_type` (`ide_device_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `live`
--

DROP TABLE IF EXISTS `live`;
CREATE TABLE IF NOT EXISTS `live` (
  `id_user` int(11) NOT NULL,
  `id_appart` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_appart`),
  KEY `id_appart` (`id_appart`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `lodger`
--

DROP TABLE IF EXISTS `lodger`;
CREATE TABLE IF NOT EXISTS `lodger` (
  `id_lodger` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id_lodger`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `owner`
--

DROP TABLE IF EXISTS `owner`;
CREATE TABLE IF NOT EXISTS `owner` (
  `id_owner` int(11) NOT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_owner`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id_region` varchar(50) NOT NULL,
  `region_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `resource`
--

DROP TABLE IF EXISTS `resource`;
CREATE TABLE IF NOT EXISTS `resource` (
  `id_ressource` int(11) NOT NULL,
  `emission` varchar(50) DEFAULT NULL,
  `_max` int(11) DEFAULT NULL,
  `_min` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ressource`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `substance`
--

DROP TABLE IF EXISTS `substance`;
CREATE TABLE IF NOT EXISTS `substance` (
  `id_substance` int(11) NOT NULL,
  `_max` int(11) DEFAULT NULL,
  `_min` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_substance`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type_appartment`
--

DROP TABLE IF EXISTS `type_appartment`;
CREATE TABLE IF NOT EXISTS `type_appartment` (
  `id_type_appartment` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type_appartment`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type_piece`
--

DROP TABLE IF EXISTS `type_piece`;
CREATE TABLE IF NOT EXISTS `type_piece` (
  `id_type_piece` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_type_piece`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `date_creation` varchar(50) DEFAULT NULL,
  `account_status` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `email`, `date_creation`, `account_status`, `age`, `first_name`, `last_name`, `gender`, `admin`, `phone_number`, `password`) VALUES
(1, '$email', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(2, 'karamchlayouit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(3, '$email', NULL, NULL, 5, '$f_name', '$l_name', '$gender', NULL, 5, ''),
(4, 'karamchlayouit@gmail.com', NULL, NULL, 5, '', '', '', NULL, 5, ''),
(5, 'karamchlayouit@gmail.com', NULL, NULL, 21, 'karam', 'chlayouit', 'm', NULL, 6, ''),
(6, '$email', NULL, NULL, 5, '$f_name', '$l_name', '$gender', NULL, 5, '$password'),
(7, '$email', NULL, NULL, 5, '$f_name', '$l_name', '$gender', NULL, 10, '$password'),
(8, 'karamchlayouit@gmail.com', NULL, NULL, NULL, '', '', '', NULL, NULL, '5'),
(9, 'karamchlayouit@gmail.com', NULL, NULL, 5, '', '', '', NULL, 5, '5'),
(10, 'karamchlayouit@gmail.com', NULL, NULL, 5, '', '', '', NULL, 5, '5'),
(11, 'karamchlayouit@gmail.com', NULL, NULL, 20, 'karam', 'chlayouit', 'm', NULL, 6, 'karam');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
